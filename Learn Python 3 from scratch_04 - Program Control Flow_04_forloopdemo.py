"""
Execute statements repeatedly
Conditions are used to stop the execution of loops
Iterable items are Strings, List, Tuple, Dictionary
"""
# Strings for loop
print('----- STRING for loop -----')
my_string = 'abcabc'
print('my_string starts off as: "' + my_string + '"')

print('Then we substitue "a" with "A" ')
print('Adding "and=' '" prints on a new line, each time')
for c in my_string:
    if c == 'a':
        print('A', end=' ')
    else:
        print(c, end=' ')

print()

# List for loop
print('----- LIST for loop -----')
cars = ['bmw', 'benz', 'honda', 'opel', 'toyota']

for car in cars:
    print(car)

nums = [1, 2, 3]
for n in nums:
    print(n * 10)

print("*"*20)


# Dictionary for loop
print('----- DICT for loop -----')
d = {'one': 1, 'two': 2, 'three': 3}
for k in d:
    print(k + " " + str(d[k]))

print("*"*20)

for k, v in d.items():
    print(k, "is: ", end=' ')
    print(v, end=' ')
