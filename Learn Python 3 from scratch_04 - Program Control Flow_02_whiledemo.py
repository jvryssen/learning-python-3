"""
Execute statements repeatedly
Conditions are used to stop the execution of loops
Iterable items are Strings, List, Tuple, Dictionary
"""
# x = 0
# while x <= 10:
#     print("Value of x is: " + str(x))
#     x = x + 1
# print()

theList001 = ['A', 'B', 'C']
num = 1
while num <= 10:
    theList001.append(num * 3.5)
    print("Value of num is: " + str(num) + " x 3.5 is... see below")
    num += 1

print()
print('mylist is: ' + str(theList001))
print()
print('Num is: ' + str(num) + ' so the loop exited')
print()
print('0th value from mylist is still: ' + str(theList001[0]))
print('1st value from mylist is still: ' + str(theList001[1]))
print('2nd value from mylist is still: ' + str(theList001[2]))
print()
print('1st value from num is now: ' + str(theList001[3]))
print('2nd value from num is now: ' + str(theList001[4]))
print('3rd value from num is now: ' + str(theList001[5]))
print('4th value from num is now: ' + str(theList001[6]))
print('5th value from num is now: ' + str(theList001[7]))
